"use strict";
// 1. Браузер по вказаній URL адресі відправляє запит і отримує (завантажує) з сервера веб-сторінку у вигляді HTML коду, 
//     який часто називається вихідний код сторінки. 
//     І якщо у коді вказані інші файли такі як стилі css, js - то завантажує і їх.
//     Браузер створює DOM для того щоб за допомогою JavaScript можна було швидко маніпулювати веб-документом: 
//     шукати потрібний елемент, додавати нові елементи, отримати наступний дочірний елемент і т.п..
// 2. innerText - показує текст, який знаходиться між відкриваючими і закриваючими тегами елементу. 
//    innerHTML - покаже текстову інформацію рівно по одному елементу. В висновок попаде і текст і розмітка документу. 
// 3. Звернутись до елементу можна послідовно перемістившись по струткурі об"єкту до самого елементу 
//     або напряму звернутись до елементу за лопомогою його ідентифікатору або тегу.

let paragraphs = document.querySelectorAll('p');
paragraphs.forEach(el => {
    el.style.backgroundColor = "#ff0000";
});
console.log(paragraphs);

let optionalList = document.querySelector("#optionsList");
console.log(optionalList);
console.log(optionalList.parentElement);
for (let i = 0; i < optionalList.children.length; i++) {
    console.log(optionalList.children[i].nodeName + " " + typeof optionalList.children[i]);
}

let tParagraph = document.querySelector("#testParagraph");
tParagraph.innerHTML = "This is a paragraph";

let elements = document.querySelector('.main-header');
 for (let i = 0; i < elements.children.length; i++) {
    elements.children[i].classList.add("nav-item");  
    console.log(elements.children[i].classList);
 }

let sectionEl = document.querySelectorAll('.section-title');
 for (let i = 0; i < sectionEl.length; i++) {
    sectionEl[i].classList.remove("section-title");
    console.log(sectionEl[i].classList);
 }

// let elemSections = document.querySelector(".section-title");
// elemSections.

